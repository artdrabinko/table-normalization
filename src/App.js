import React from "react";
import { applyMiddleware, createStore } from "redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { rootReducer } from "./ducks";
import { Main } from "./layouts/Main";
import { DependenciesStructure } from "./layouts/DependenciesStructure";
import { Result } from "./layouts/Result";
import { About } from "./components/About";
import { NoMatch } from "./layouts/NoMatch";

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="container">
          <div className="content-layout">
            <Switch>
              <Route path="/" exact component={Main} />
              <Route path="/structure" component={DependenciesStructure} />
              <Route path="/result" component={Result} />
              <Route path="/about" component={About} />
              <Route component={NoMatch} />
            </Switch>
          </div>
          <footer className="footer">
            <div className="has-text-centered" />
          </footer>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
