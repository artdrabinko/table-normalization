class Attribute {
  constructor(name) {
    this.name = name;

    this.getName = this.getName.bind(this);
    this.setName = this.setName.bind(this);
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
  }
}

export default Attribute;
