class FunctionalDependency {
  constructor(leftPart = [], rightPart = [], selectedAttributes = []) {
    this.leftPart = leftPart;
    this.rightPart = rightPart;
    this.selectedAttributes = selectedAttributes;

    this.getLeftPart = this.getLeftPart.bind(this);
    this.getRightPart = this.getRightPart.bind(this);
    
    this.isContaintAttribute = this.isContaintAttribute.bind(this);
    this.pushAttrToLeftPart = this.pushAttrToLeftPart.bind(this);
    this.pushAttrToLeftPart = this.pushAttrToLeftPart.bind(this);
  }

  getLeftPart() {
    return this.leftPart;
  }

  getRightPart() {
    return this.rightPart;
  }

  isContaintAttribute(part, attribute) {
    return part.includes(attribute);
  }

  pushAttrToLeftPart(attribute) {
    if (!this.isContaintAttribute(this.leftPart, attribute)) {
      this.leftPart.push(attribute);
    }
  }

  pushAttrToRightPart(attribute) {
    if (!this.isContaintAttribute(this.rightPart, attribute)) {
      this.rightPart.push(attribute);
    }
  }
}

export default FunctionalDependency;
