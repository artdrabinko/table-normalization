import { optimizeLeftFuncDepend } from "./firstStep";

export const findMainPrimaryKey = (structure, attributes) => {
  const rightPart = [...attributes];
  const filteredLeftPart = optimizeLeftFuncDepend(structure, attributes);

  return {
    leftPart: filteredLeftPart,
    rightPart
  };
};
