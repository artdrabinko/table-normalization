import { makeClosure } from "./helpers";

export const optimizeLeftFuncDepend = (structure, leftPart) => {
  const condidatesToRemove = [...leftPart];

  condidatesToRemove.forEach(attrToRemove => {
    if (leftPart.length > 1) {
      leftPart = leftPart.filter(attrubute => attrubute !== attrToRemove);

      const closure = makeClosure(structure, leftPart);

      if (!closure.includes(attrToRemove)) {
        leftPart.push(attrToRemove);
      }
    }
  });

  return [...leftPart];
};

export const optimizeLeftStructurePart = structure => {
  let processingStructure = [...structure];

  processingStructure.forEach(functDependency => {
    const { leftPart } = functDependency;
    const leftPartLength = functDependency.leftPart.length;

    if (leftPartLength > 1) {
      const optimizedLeftPart = optimizeLeftFuncDepend(
        processingStructure,
        leftPart
      );
      functDependency.leftPart = optimizedLeftPart;
    }
  });

  return [...processingStructure];
};
