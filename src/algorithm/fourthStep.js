export const removeFuncDependWithEmptyRightParts = structure => {
  const filteredStructure = structure.filter(functDependency => {
    return functDependency.rightPart.length;
  });

  return filteredStructure;
};
