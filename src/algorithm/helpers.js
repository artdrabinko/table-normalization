export const isEqualsArrays = (arrayA, arrayB) => {
  if (arrayA.length !== arrayB.length) {
    return false;
  }

  let isEquals = true;

  arrayA.forEach(itemA => {
    if (!arrayB.includes(itemA)) {
      isEquals = false;
    }
  });

  return isEquals;
};

export const isContainRelationAttrs = (processingAttrs, relations) => {
  let isContain = true;

  relations.forEach(relationAttr => {
    if (!processingAttrs.includes(relationAttr)) {
      isContain = false;
    }
  });

  return isContain;
};

export const checkMainPrimaryKey = (structure, attributes) => {
  let isContainPrimaryKey = false;
  const structureLength = structure.length;

  for (let index = 0; index < structureLength; index++) {
    let funcDependency = structure[index];
    let closure = makeClosure(structure, funcDependency.leftPart);
    
    if (closure.length === attributes.length) {
      isContainPrimaryKey = true;
      break;
    }
  }

  return isContainPrimaryKey;
};

export const multAttributesMerge = (processingClosure, attributes) => {
  const mergeResult = [...processingClosure];

  attributes.forEach(attr => {
    if (!mergeResult.includes(attr)) {
      mergeResult.push(attr);
    }
  });

  return mergeResult;
};

export const makeClosure = (structure, multipleAttributes) => {
  let attributesClosure = [...multipleAttributes];
  let isClosureModifaied = false;

  structure.forEach(functDependency => {
    const { leftPart, rightPart } = functDependency;

    if (
      isContainRelationAttrs(attributesClosure, leftPart) === true &&
      isContainRelationAttrs(attributesClosure, rightPart) === false
    ) {
      attributesClosure = multAttributesMerge(attributesClosure, rightPart);

      isClosureModifaied = true;
    }
  });

  if (isClosureModifaied) {
    attributesClosure = makeClosure(structure, attributesClosure);
  }

  return attributesClosure;
};
