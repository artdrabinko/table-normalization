import { optimizeLeftStructurePart } from "./firstStep";
import { combainEqualLeftParts } from "./secondStep";
import { optimizeRightStructurePart } from "./thirdStep";
import { removeFuncDependWithEmptyRightParts } from "./fourthStep";
import { findMainPrimaryKey } from "./fifthStep";
import { checkMainPrimaryKey } from "./helpers";

export const processStructure = (structure, attributes) => {
  const completedFirstStep = optimizeLeftStructurePart(structure);
  const completedSecondStep = combainEqualLeftParts(completedFirstStep);
  const completedThirdStep = optimizeRightStructurePart(completedSecondStep);
  const optimizedStructure = removeFuncDependWithEmptyRightParts(
    completedThirdStep
  );

  console.log("---- Step 4 ---");
  console.log(optimizedStructure);
  const isContainMainPrimaryKey = checkMainPrimaryKey(
    optimizedStructure,
    attributes
  );

  console.log("isContainMainPrimaryKey - ", isContainMainPrimaryKey);

  if (isContainMainPrimaryKey === false) {
    const newFunctionDependency = findMainPrimaryKey(
      optimizedStructure,
      attributes
    );
    optimizedStructure.push(newFunctionDependency);

    console.log("Addition function dependency", newFunctionDependency);
  }

  return optimizedStructure;
};
