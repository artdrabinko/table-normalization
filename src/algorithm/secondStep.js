import { isEqualsArrays, multAttributesMerge } from "./helpers";

export const combainEqualLeftParts = structure => {
  for (let i = 0; i < structure.length; i++) {
    const currFuncDepend = structure[i];

    for (let j = i + 1; j < structure.length; j++) {
      const nextFuncDepend = structure[j];

      if (isEqualsArrays(currFuncDepend.leftPart, nextFuncDepend.leftPart)) {
        const mergedRightParts = multAttributesMerge(
          currFuncDepend.rightPart,
          nextFuncDepend.rightPart
        );
        currFuncDepend.rightPart = mergedRightParts;
        structure.splice(j, 1);
        j--;
      }
    }
  }

  return [...structure];
};
