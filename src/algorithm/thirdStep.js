import { makeClosure } from "./helpers";

const optimizeRightFuncDepend = (structure, structureIndex) => {
  const { leftPart } = structure[structureIndex];

  for (
    let index = 0;
    index < structure[structureIndex].rightPart.length;
    index++
  ) {
    const attrToRemove = structure[structureIndex].rightPart.shift();

    const closure = makeClosure(structure, leftPart);

    if (!closure.includes(attrToRemove)) {
      structure[structureIndex].rightPart.push(attrToRemove);
    } else {
      index--;
    }
  }

  return [...structure[structureIndex].rightPart];
};

export const optimizeRightStructurePart = structure => {
  let processingStructure = [...structure];

  for (let index = 0; index < processingStructure.length; index++) {
    const optimizedRightPart = optimizeRightFuncDepend(
      processingStructure,
      index
    );

    processingStructure[index].rightPart = optimizedRightPart;
  }

  return [...processingStructure];
};
