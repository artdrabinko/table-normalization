import React from "react";
import { Link } from "react-router-dom";

function About() {
  return (
    <section className="columns">
      <div className="column is-8 is-offset-2">
        <Link to="/" className="button is-primary m-b-sm">
          На главную
        </Link>
        <h3 className="title is-3">О программе</h3>
        <p className="box">
          Разработчик: Корнейчук Д. И.
          <br />
          Краткая инструкция :<br />
          На первоначальном этапе ввродятся имена всех атрибутов, используемых в
          базе данных. Имя атрибута должно содержать от 2 до 255 символов. При
          некорректном вводе имени атрибута, следует нажать на значок{" "}
          <button class="delete is-small has-background-grey" />, который
          находится в правом верхнем углу над именем атрибута. На втором этапе
          вводятся все функциональные зависимости. Правые и левые части
          функциональных зависимостей должны быть непустыми. Функциональные
          зависимости не должны дублироваться. Для удаления атрибута из
          функциональной зависимости нужно нажать на значок{" "}
          <button class="delete is-small has-background-grey" />в правом верхнем
          углу над именем атрибута. Для удаления функциональной зависимости
          нужно нажать на кнопку <span class="tag is-danger">Удалить</span>,
          которая расположена слева от функциональной зависимости. Результатом
          выполнения программы является структура таблиц базы данных,
          приведённая к третьей нормальной форме.
        </p>
      </div>
    </section>
  );
}

export default About;
