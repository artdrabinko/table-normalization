import React, { Component } from "react";
import { connect } from "react-redux";
import { destroyAttribute } from "../../ducks/attributes";

export class Attribute extends Component {
  render() {
    const { value, destroyAttribute } = this.props;
    return (
      <div className="m-b-sm m-r-sm">
        <span className="tag is-size-6 is-info">
          {value}
          <button
            className="delete is-small"
            onClick={() => {
              destroyAttribute(value);
            }}
          />
        </span>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  destroyAttribute
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Attribute);
