import React, { Component } from "react";
import { connect } from "react-redux";
import { Attribute } from "../Attribute";

export class AttributeList extends Component {
  renderList() {
    const { attributes } = this.props;
    return attributes.map((value, index) => {
      return <Attribute key={index} value={value} />;
    });
  }
  render() {
    const { attributes } = this.props;
    return attributes.length ? (
      <div className="attribute-list-container p-t-md p-b-md">
        <div className="attribute-list box">{this.renderList()}</div>
      </div>
    ) : (
      ""
    );
  }
}

const mapStateToProps = state => ({
  attributes: state.attributes
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AttributeList);
