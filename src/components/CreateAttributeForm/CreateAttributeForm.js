import React, { Component } from "react";
import { connect } from "react-redux";
import { storeAttribute } from "../../ducks/attributes";

export class CreateAttributeForm extends Component {
  constructor(props) {
    super(props);
    this.state = { attributeValue: "", showError: false, errorMessage: "" };
  }

  showInputErrorMessage = () => {
    this.setState({ showError: true });
  };

  hideInputErrorMessage = () => {
    this.setState({ showError: false });
  };

  isValidateValue = value => {
    const { attributes } = this.props;

    if (value.length < 2 || value.length > 64) {
      this.setState({
        errorMessage: "Название атрибута должно быть от 2-х до 64-х символов!"
      });
      return false;
    } else if (attributes.includes(value)) {
      this.setState({
        errorMessage: "Атрибут уже существует!"
      });
      return false;
    } else {
      this.setState({
        errorMessage: ""
      });
      return true;
    }
  };

  onSubmit = event => {
    event.preventDefault();
    const { attributeValue } = this.state;

    if (this.isValidateValue(attributeValue)) {
      this.props.storeAttribute(attributeValue);
      this.resetForm();
    } else {
      this.showInputErrorMessage();
    }
  };

  resetForm = () => {
    this.hideInputErrorMessage();
    this.setState({
      attributeValue: ""
    });
  };

  onChange = event => {
    const { value } = event.target;
    this.setState({
      attributeValue: value
    });
  };

  render() {
    const { showError, errorMessage } = this.state;
    console.log(showError);

    return (
      <form onSubmit={this.onSubmit}>
        <div className="field is-grouped">
          <div className="control is-expanded">
            <input
              className={showError ? "input is-danger" : "input"}
              type="text"
              placeholder="Название атрибута"
              onChange={this.onChange}
              value={this.state.attributeValue}
            />
            {showError ? <p className="help is-danger">{errorMessage}</p> : ""}
          </div>
          <div className="control">
            <button className="button is-success" type="submit">
              Добавить
            </button>
          </div>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  attributes: state.attributes
});

const mapDispatchToProps = {
  storeAttribute
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateAttributeForm);
