import React, { Component } from "react";
import { connect } from "react-redux";
import { addAttrToFuncDependency } from "../../ducks/structure";

export class DropdownAttributes extends Component {
  filterOptions = () => {
    const { structure, attributes, rowIndex } = this.props;
    const { selectedAttributes } = structure[rowIndex];

    return attributes.filter(attrName => {
      return !selectedAttributes.includes(attrName);
    });
  };

  handleSelectAttribute = attrName => {
    const { rowIndex, partPosition } = this.props;
    this.props.addAttrToFuncDependency({ rowIndex, partPosition, attrName });
  };

  renderOptions = attributes => {
    return attributes.map((attrName, index) => {
      return (
        <span
          key={index}
          onClick={() => {
            this.handleSelectAttribute(attrName);
          }}
          className="tag is-info m-xs"
        >
          {attrName}
        </span>
      );
    });
  };

  render() {
    const filteredAttributes = this.filterOptions();
    const isContainAttributes = filteredAttributes.length;
    return (
      <div className="dropdown is-right is-hoverable">
        <div className="dropdown-trigger">
          <button
            className="button"
            aria-haspopup="true"
            aria-controls="dropdown-menu4"
          >
            <span>
              {isContainAttributes ? "Выберите атрибут" : "Пусто ..."}
            </span>
            <span className="icon is-small">
              <i className="fas fa-angle-down" aria-hidden="true" />
            </span>
          </button>
        </div>
        {isContainAttributes ? (
          <div className="dropdown-menu" role="menu">
            <div className="dropdown-content p-l-sm p-r-sm">
              {this.renderOptions(filteredAttributes)}
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  attributes: state.attributes,
  structure: state.structure
});

const mapDispatchToProps = {
  addAttrToFuncDependency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DropdownAttributes);
