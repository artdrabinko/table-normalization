import React, { Component } from "react";
import { connect } from "react-redux";
import { DropdownAttributes } from "../DropdownAttributes";
import { MultipleAttributes } from "../MultipleAttributes";
import { removeFuncDependency } from "../../ducks/structure";

export class FunctionalDependency extends Component {
  onRemove = () => {
    const { rowIndex, removeFuncDependency } = this.props;
    removeFuncDependency(rowIndex);
  };

  render() {
    const { rowIndex, data } = this.props;
    const { leftPart, rightPart } = data;

    return (
      <tr>
        <th>{rowIndex + 1}</th>
        <td>
          <DropdownAttributes
            rowIndex={rowIndex}
            items={leftPart}
            partPosition={"leftPart"}
          />
          <MultipleAttributes rowIndex={rowIndex} partPosition={"leftPart"} />
        </td>
        <td>
          <DropdownAttributes
            rowIndex={rowIndex}
            items={rightPart}
            partPosition={"rightPart"}
          />
          <MultipleAttributes rowIndex={rowIndex} partPosition={"rightPart"} />
        </td>
        <td>
          <button className="button is-danger" onClick={this.onRemove}>
            Удалить
          </button>
        </td>
      </tr>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  removeFuncDependency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FunctionalDependency);
