import React, { Component } from "react";
import { connect } from "react-redux";
import { removeAttrFromFuncDependency } from "../../ducks/structure";

export class MultipleAttributes extends Component {
  onRemove = attrName => {
    const { rowIndex, partPosition } = this.props;
    this.props.removeAttrFromFuncDependency({
      rowIndex,
      partPosition,
      attrName
    });
  };

  renderMultiple = () => {
    const { structure, rowIndex, partPosition } = this.props;
    const items = structure[rowIndex][partPosition];

    return items.map((attrName, index) => {
      return (
        <span key={index} className="tag is-size-6 is-info m-t-sm m-r-sm">
          {attrName}
          <button
            className="delete is-small"
            onClick={() => {
              this.onRemove(attrName);
            }}
          />
        </span>
      );
    });
  };

  render() {
    return <div>{this.renderMultiple()}</div>;
  }
}

const mapStateToProps = state => ({
  structure: state.structure
});

const mapDispatchToProps = {
  removeAttrFromFuncDependency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MultipleAttributes);
