import { createAction, createReducer } from "redux-act";

export const REDUCER = "ATTRIBUTES";

const NS = `${REDUCER}__`;

const initialState = [];

const reducer = createReducer({}, initialState);

const readRequest = createAction(`${NS}READ_REQUEST`);
reducer.on(readRequest, state => [...state]);

const storeRequest = createAction(`${NS}STORE_REQUEST`);
reducer.on(storeRequest, (state, newItem) => [...state, newItem]);

const destroyRequest = createAction(`${NS}DESTROY_REQUEST`);
reducer.on(destroyRequest, (state, removingItem) => {
  return state.filter(item => {
    return item !== removingItem;
  });
});

export const readAttributes = () => dispatch => {
  dispatch(readRequest());
};

export const storeAttribute = newAttribute => dispatch => {
  dispatch(storeRequest(newAttribute));
};

export const destroyAttribute = attribute => dispatch => {
  dispatch(destroyRequest(attribute));
};

export default reducer;
