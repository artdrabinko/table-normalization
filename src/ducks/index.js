import { combineReducers } from "redux";
import attributes from "./attributes";
import structure from "./structure";
import tables from "./tables";

export const rootReducer = combineReducers({
  attributes,
  structure,
  tables
});
