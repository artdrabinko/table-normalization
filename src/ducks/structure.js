import { createAction, createReducer } from "redux-act";
import FunctionalDependency from "../algorithm/FunctionalDependency";

export const REDUCER = "STRUCTURE";

const NS = `${REDUCER}__`;

const initialState = [];

const reducer = createReducer({}, initialState);

const readRequest = createAction(`${NS}READ_REQUEST`);
reducer.on(readRequest, state => [...state]);

const storeRequest = createAction(`${NS}STORE_REQUEST`);
reducer.on(storeRequest, (state, newItem) => [...state, newItem]);

const updateRequest = createAction(`${NS}UPDATE_REQUEST`);
reducer.on(updateRequest, (state, newItem) => {
  const updatedState = [...state, newItem];
  return updatedState;
});

const destroyRequest = createAction(`${NS}DESTROY_REQUEST`);
reducer.on(destroyRequest, (state, removingIndex) => {
  return state.filter((item, index) => {
    return index !== removingIndex;
  });
});

const removeAttrFromFuncDependPart = createAction(`${NS}REMOVE_SELECTED_ATTR_FUNC_DEPEND_PART`);
reducer.on(removeAttrFromFuncDependPart, (state, { rowIndex, partPosition, attrName }) => {
  return state.map((funcDepend, index) => {
    if (index === rowIndex) {
      if (funcDepend.selectedAttributes.includes(attrName)) {
        funcDepend.selectedAttributes = funcDepend.selectedAttributes.filter(attr => attr !== attrName);
        funcDepend[partPosition] = funcDepend[partPosition].filter(attr => attr !== attrName);
      }
    }

    return funcDepend;
  });
});

const addAttrToFuncDependPart = createAction(`${NS}ADD_ATTR_FUNC_DEPEND_PART`);
reducer.on(addAttrToFuncDependPart, (state, { rowIndex, partPosition, attrName }) => {
  return state.map((funcDepend, index) => {
    if (index === rowIndex) {
      if (!funcDepend.selectedAttributes.includes(attrName)) {
        funcDepend.selectedAttributes.push(attrName);
        funcDepend[partPosition].push(attrName);
      }
    }

    return funcDepend;
  });
});

export const readStructure = () => dispatch => {
  dispatch(readRequest());
};

export const addFuncDependency = () => dispatch => {
  dispatch(storeRequest(new FunctionalDependency()));
};

export const removeFuncDependency = funcDependencyIndex => dispatch => {
  dispatch(destroyRequest(funcDependencyIndex));
};

export const addAttrToFuncDependency = (payload) => dispatch => {
  dispatch(addAttrToFuncDependPart(payload));
};

export const removeAttrFromFuncDependency = (payload) => dispatch => {
  dispatch(removeAttrFromFuncDependPart(payload));
};

export default reducer;
