import { createAction, createReducer } from "redux-act";
import { processStructure } from "../algorithm/main";
import {} from "../algorithm/FunctionalDependency";
export const REDUCER = "TABLES";

const NS = `${REDUCER}__`;

const initialState = [];

const reducer = createReducer({}, initialState);

const readRequest = createAction(`${NS}READ_REQUEST`);
reducer.on(readRequest, state => [...state]);

const updateRequest = createAction(`${NS}UPDATE_REQUEST`);
reducer.on(updateRequest, (state, tables) => [...tables]);

const cloneStructure = cloningStructure => {
  const clonedStructure = cloningStructure.map(structure => {
    const { leftPart, rightPart } = structure;
    const clonedLeftPart = leftPart.map(attribute => attribute);
    const clonedRightPart = rightPart.map(attribute => attribute);

    return {
      leftPart: clonedLeftPart,
      rightPart: clonedRightPart
    };
  });
  return clonedStructure;
};

export const optimizeStructure = (structure, attributes) => dispatch => {
  const clonedStructure = cloneStructure(structure);
  const processedStructure = processStructure(clonedStructure, [...attributes]);
  dispatch(updateRequest(processedStructure));
};

export default reducer;
