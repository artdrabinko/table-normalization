import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import { FunctionalDependency } from "../../components/FunctionalDependency";
import { addFuncDependency } from "../../ducks/structure";

export class DependenciesStructure extends Component {
  isValidStructure = () => {
    let isValid = true;
    const { structure } = this.props;
    structure.forEach(funcDependency => {
      if (
        funcDependency.leftPart.length < 1 ||
        funcDependency.rightPart.length < 1
      ) {
        isValid = false;
      }
    });

    return isValid;
  };

  renderAttributes = () => {
    const { attributes } = this.props;

    return attributes.map((item, index) => {
      return (
        <span key={index} className="tag is-info">
          {item}
        </span>
      );
    });
  };

  renderStructure = () => {
    const { structure } = this.props;

    return structure.map((funcDependency, index) => {
      return (
        <FunctionalDependency
          key={index}
          rowIndex={index}
          data={funcDependency}
        />
      );
    });
  };

  render() {
    const { attributes, structure } = this.props;
    if (attributes.length === 0) {
      return <Redirect to="/" />;
    }
    return (
      <section className="columns">
        <div className="column is-8 is-offset-2">
          <header className="level">
            <div className="level-left">
              <div className="level-item">
                <Link to="/" className="button is-primary m-b-sm">
                  Назад
                </Link>
              </div>
            </div>
            <div className="level-right">
              <div className="level-item">
                <Link to="/about" className="">
                   Справка
                  <i class="far fa-question-circle m-l-sm" />
                </Link>
              </div>
            </div>
          </header>
          <header className="level">
            <div className="level-left">
              <div className="level-item">
                <h4 className="title is-4">Структура зависимостей</h4>
              </div>
            </div>
            <div className="level-right">
              <div className="level-item">
                <button
                  className="button is-success"
                  onClick={this.props.addFuncDependency}
                >
                  Добавить
                </button>
              </div>
            </div>
          </header>

          <div className="">
            <table className="table is-bordered is-fullwidth m-b-md">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Левая часть</th>
                  <th>Правая часть</th>
                  <th />
                </tr>
              </thead>
              <tbody>{this.renderStructure()}</tbody>
            </table>
          </div>
          {structure.length && this.isValidStructure() ? (
            <Link to="/result" className="button is-primary">
              <i className="fas fa-bolt has-text-warning m-r-sm" />
              Оптимизировать
            </Link>
          ) : (
            ""
          )}
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  attributes: state.attributes,
  structure: state.structure
});

const mapDispatchToProps = {
  addFuncDependency
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DependenciesStructure);
