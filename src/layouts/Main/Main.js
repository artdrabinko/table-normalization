import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { AttributeList } from "../../components/AttributeList";
import { CreateAttributeForm } from "../../components/CreateAttributeForm";

function Main({ attributes }) {
  return (
    <section className="columns">
      <div className="column is-8 is-offset-2">
        <header className="level">
          <div className="level-left">
            <div className="level-item">
              <h3 className="title is-3">Создание атрибутов</h3>
            </div>
          </div>
          <div className="level-right">
            <div className="level-item">
              <Link to="/about" className="">
                 Справка
                <i class="far fa-question-circle m-l-sm" />
              </Link>
            </div>
          </div>
        </header>
        <CreateAttributeForm />
        <AttributeList />
        {attributes.length ? (
          <Link to="/structure" className="button is-primary">
            Далее
            <i className="fas fa-arrow-right m-l-sm" />
          </Link>
        ) : (
          ""
        )}
      </div>
    </section>
  );
}

const mapStateToProps = state => ({
  attributes: state.attributes
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
