import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { optimizeStructure } from "../../ducks/tables";

export class Result extends Component {
  componentDidMount() {
    const { structure, attributes } = this.props;
    this.props.optimizeStructure(structure, attributes);
  }

  renderRows(rows, isPrimary) {
    return rows.map((row, index) => {
      return (
        <div key={index} className="result-table__row">
          {isPrimary ? (
            <i className="fas fa-key key-icon m-r-sm has-text-warning" />
          ) : (
            ""
          )}
          <span>{row}</span>
        </div>
      );
    });
  }

  renderTable(table, index) {
    return (
      <div key={index} className="result-table">
        {this.renderRows(table.leftPart, true)}
        {this.renderRows(table.rightPart, false)}
      </div>
    );
  }

  renderTables() {
    return this.props.tables.map((table, index) => {
      return (
        <div key={index}>
          <h5 className="table-header">Таблица {index + 1}</h5>
          {this.renderTable(table, index)}
        </div>
      );
    });
  }
  render() {
    return (
      <section className="columns">
        <div className="column is-8 is-offset-2">
          <Link to="/structure" className="button is-primary m-b-sm">
            Назад
          </Link>
          <header className="level">
            <div className="level-left">
              <div className="level-item">
                <h4 className="title is-4">Результат оптимизации</h4>
              </div>
            </div>
            <div className="level-right">
              <div className="level-item" />
            </div>
          </header>
          <div className="table-area">{this.renderTables()}</div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  attributes: state.attributes,
  structure: state.structure,
  tables: state.tables
});

const mapDispatchToProps = {
  optimizeStructure
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Result);
